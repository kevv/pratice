package poker

// Game interface
type Game interface {
	Start(numberOfPlayers int)
	Finish(winner string)
}
