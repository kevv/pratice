package poker

import (
	"fmt"
	"os"
	"time"
)

// BlindAlerter is an interface for an alerter
type BlindAlerter interface {
	ScheduleAlertAt(duration time.Duration, amount int)
}

// BlindAlerterFunc the scheme of a alert function
type BlindAlerterFunc func(duration time.Duration, amount int)

// ScheduleAlertAt programs an alert
func (a BlindAlerterFunc) ScheduleAlertAt(duration time.Duration, amount int) {
	a(duration, amount)
}

// StdOutAlerter calls afterFunc func
func StdOutAlerter(duration time.Duration, amount int) {
	time.AfterFunc(duration, func() {
		fmt.Fprintf(os.Stdout, "Blind is now %d\n", amount)
	})
}
