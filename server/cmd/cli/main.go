package main

import (
	"fmt"
	"log"
	"os"

	poker "github.com/kevv/server"
)

const dbFileName = "game.db.json"

func main() {
	store, clear, err := poker.FileSystemPlayerStoreFromFile(dbFileName)
	if err != nil {
		log.Fatal(err)
	}
	defer clear()

	game := poker.NewTexasHoldem(poker.BlindAlerterFunc(poker.StdOutAlerter), store)
	cli := poker.NewCLI(os.Stdin, os.Stdout, game)

	fmt.Println("Let's play poker")
	fmt.Println("Type '{Name} wins' to record a win")
	cli.PlayPoker()
}
