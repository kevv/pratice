package main

import (
	"log"
	"net/http"

	poker "github.com/kevv/server"
)

const dbFileName = "game.db.json"

func main() {
	store, clear, err := poker.FileSystemPlayerStoreFromFile(dbFileName)
	if err != nil {
		log.Fatal(err)
	}
	defer clear()

	server := poker.NewPlayerServer(store)
	if err := http.ListenAndServe(":5000", server); err != nil {
		log.Fatalf("Could not listen on port 5000 %v", err)
	}
}
